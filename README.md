# NetSkope API events retriever

This script allows you to retrieve events from the NetSkope Platform API.

The retrieved events can be :

- Saved to .json files (default behavior)
- Send to a Cortex HTTP event collector (using `-fw` argument)

## How to use

Clone the repo :

```bash
git clone git@gitlab.com:rustaman/netskope-api-events-retriever.git
# or
git clone https://gitlab.com/rustaman/netskope-api-events-retriever.git
```

Install the required modules:

```bash
pip install -r requirements.txt

# On windows, truststore may be required if behind an ssl inspection proxy
pip install -r requirements.txt --use-feature=truststore
```

Copy the `.env.example` file :

```bash
cp .env.example .env
```

Complete the `.env` file, replace the template values with yours:

```bash
NETSKOPE_TENANT_URI='https://<tenant-name>.goskope.com'
NETSKOPE_API_KEY='<api-key>'
# Optional variables, only if you need the associated feature
CORTEX_XDR_TENANT_API_URL='https://api-<tenant-external-url>'
CORTEX_XDR_TENANT_API_TOKEN='<api-key>'
CAFILE=''
```

Launch the program with `--help` argument to see usage:

```bash
python get_events.py --help
```
