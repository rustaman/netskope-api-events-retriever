# File : get_events.py
# Author: Amin GHELIS
# Date: 30/08/2023

import os
import sys
import enum
import json
import gzip
from time import time
from requests import request, exceptions as request_exceptions
from argparse import ArgumentParser
from dotenv import load_dotenv

# Enum to represent time ranges


class TimeRange(enum.Enum):
    TR_5M = ('5m', 300)
    TR_30M = ('30m', 1800)
    TR_60M = ('60m', 3600)
    TR_24H = ('24h', 86400)
    TR_7D = ('7d', 604800)
    TR_30D = ('30d', 2592000)


class NetSkopeAPIEndpoints(enum.Enum):
    EVENTS = "events"


class NetSkopeAPI:
    def __init__(self, tenant_url: str, api_key: str):
        self.tenant_url = tenant_url
        self.api_key = api_key

    @staticmethod
    def _query(base_url: str, endpoint: str, params: dict, api_key: str) -> object:
        """Perform a query to the NetSkope API and return the result

        Args:
            base_url (str): API base url, ex: https://<tenant-name>.goskope.com/api/v1

            endpoint (str): API endpoints, ex: events

            params (dict): Query params, ex: { "type":'application', "timeperiod": 3600 }

            api_key (str): The API Key

        Returns:
            dict: result returned from the NetSkope API
        """

        # Generating the params string, ( ex: "type=application&timeperiod=3600")
        params_str = "&".join(
            [f"{key}={str(value)}" for key, value in params.items()])

        response = request(
            method='POST', url=f"{base_url}/{endpoint}?{params_str}", params={"token": api_key}, verify=CAFILE)

        try:
            if (response.ok):
                # Can raise JSONDecodeError
                data = response.json()

                # retrieving the query response status and check if no error
                if "status" in data:
                    # Handle error status
                    if data.get("status") == 'error':
                        raise ValueError(
                            f"API return error : {data['errorCode']} - {','.join(data['errors'])}")
                    # Handle success status
                    elif data.get('status') == 'success':
                        return data['data']

                    # Handle value not expected
                    else:
                        raise ValueError(
                            f"Unexpected value for \"status\" field receive inside the API response, found value: {data.get('status')}")

                # Case status field not in API response
                else:
                    raise KeyError(
                        "\"status\" field not present in API response, cannot parse the data")

            # Case response not ok
            raise Exception(
                f"Error from the api : {response.status_code} - {response.text}")

        except request_exceptions.JSONDecodeError:
            print("Cannot decode response from the API, exiting..")
            exit(1)

        except Exception as error:
            print(
                str(error), sys.stderr)
            exit(1)

    @staticmethod
    def get_relative_time_query_params(timerange: TimeRange) -> int:
        """Computes and returns the current timestamp and the current timestamp minus the provided timerange.
           Timestamps are returned in seconds.

        Args:
            timerange (TimeRange): timerange we want to substract from the current time

        Returns:
            object: {
                starttime: current timestamp minus the provided timerange
                endtime: current timestamp
            }
        """
        end_timestamp = int(time())
        start_timestamp = end_timestamp - timerange.value[1]

        return {
            "starttime": start_timestamp,
            "endtime": end_timestamp
        }

    def get_app_events(self, timerange: TimeRange) -> list:
        """Retrieve application type events from the NetSkope API

        Args:
            timerange (TimeRange): The timerange of the events (See Enum type \"TimeRange\")

        Returns:
            list: events
        """

        params = NetSkopeAPI.get_relative_time_query_params(timerange)

        params["type"] = "application"

        results = NetSkopeAPI._query(
            self.tenant_url, NetSkopeAPIEndpoints.EVENTS.value, params, self.api_key)

        return results

    def get_page_events(self, timerange: TimeRange) -> list:
        """Retrieve page type events from the NetSkope API

        Args:
            timerange (TimeRange): The timerange of the events (See Enum type \"TimeRange\")

        Returns:
            list: events
        """

        params = NetSkopeAPI.get_relative_time_query_params(timerange)

        params["type"] = "page"

        results = NetSkopeAPI._query(
            self.tenant_url, NetSkopeAPIEndpoints.EVENTS.value, params, self.api_key)

        return results

    def get_audit_events(self, timerange: TimeRange) -> list:
        """Retrieve audit type events from the NetSkope API

        Args:
            timerange (TimeRange): The timerange of the events (See Enum type \"TimeRange\")

        Returns:
            list: events
        """

        params = NetSkopeAPI.get_relative_time_query_params(timerange)

        params["type"] = "audit"

        results = NetSkopeAPI._query(
            self.tenant_url, NetSkopeAPIEndpoints.EVENTS.value, params, self.api_key)

        return results

    def get_infra_events(self, timerange: TimeRange) -> list:
        """Retrieve infrastructure type events from the NetSkope API

        Args:
            timerange (TimeRange): The timerange of the events (See Enum type \"TimeRange\")

        Returns:
            list: events
        """

        params = NetSkopeAPI.get_relative_time_query_params(timerange)

        params["type"] = "infrastructure"

        results = NetSkopeAPI._query(
            self.tenant_url, NetSkopeAPIEndpoints.EVENTS.value, params, self.api_key)

        return results

    def get_network_events(self, timerange: TimeRange) -> list:
        """Retrieve network type events from the NetSkope API

        Args:
            timerange (TimeRange): The timerange of the events (See Enum type \"TimeRange\")

        Returns:
            list: events
        """

        params = NetSkopeAPI.get_relative_time_query_params(timerange)

        params["type"] = "network"

        results = NetSkopeAPI._query(
            self.tenant_url, NetSkopeAPIEndpoints.EVENTS.value, params, self.api_key)

        return results


class CortexHTTPCollector:
    def __init__(self, collector_url: str, authorization_token: str) -> None:
        self.collector_url = collector_url + "/logs/v1/event"
        self.authorization_token = authorization_token

    def send_json_data(self, data: list) -> None:
        """Send the provided JSON serializable data to the provided HTTP Collector URL

        Args:
            url (str): URL of the HTTP Collector
            authorization (str): Authorization Token
            data (list): List of python dictionnaries, the data to send
        """

        headers = {
            "Authorization": self.authorization_token,
            "Content-Type": "application/json"
        }

        # Generating the body as json line separated entries
        body = ""
        for entry in data:
            try:
                body += f"{json.dumps(entry)}\n"
            except TypeError as e:
                print(
                    f"Not able to serialize entry {str(entry)}, proceeding..", file=sys.stderr)

        # Compressing the body to be accepted by the API
        headers['Content-Type'] = "gzip"
        compressed_body = gzip.compress(body.encode('utf-8'))

        # Sending the events
        try:
            response = request(method="POST", url=self.collector_url,
                               headers=headers, data=compressed_body)

            if not response.ok:
                raise ValueError(
                    f"Error while sending events to the HTTP Collector : {response.status_code} - {response.text}")

        except Exception as error:
            print(str(error), file=sys.stderr)
            exit(1)


# Loading .env file
load_dotenv()
try:
    # Will raise if TENANT_URI cannot be loaded
    NETSKOPE_TENANT_API_URL: str = os.getenv("NETSKOPE_TENANT_URI") + "/api/v1"
    API_KEY: str = os.getenv("NETSKOPE_API_KEY")
    # Raising if API_KEY cannot be loaded
    if (API_KEY is None):
        raise ValueError()
    # This one is optionnal
    CAFILE: str = os.getenv("CAFILE")
    if (CAFILE is None):
        print("Info: No CA file were loaded. If you are reaching the NetSkope API behind a proxy that performs SSL traffic inspection, please make sure to provide a CA File in the .env file")

    # Cortex XDR HTTP Collector config load
    CORTEX_XDR_TENANT_API_URL: str = os.getenv("CORTEX_XDR_TENANT_API_URL")
    if (CORTEX_XDR_TENANT_API_URL is None):
        print("Info: No Cortex XDR HTTP Collector URL provided, if you intend to forward events directly to the Cortex XDR HTTP Collector, please make sure to provide a Cortex XDR HTTP Collector URL in the .env file")

    CORTEX_XDR_TENANT_API_TOKEN: str = os.getenv("CORTEX_XDR_TENANT_API_TOKEN")
    if (CORTEX_XDR_TENANT_API_TOKEN is None and CORTEX_XDR_TENANT_API_URL is not None):
        raise ValueError(
            "Error: Cortex XDR HTTP Collector URL provided but not API Token provided, please make sure to provide a Cortex XDR HTTP Collector Token in the .env file")


except Exception as error:
    print(
        f"Cannot load environment variables, please check that you have a valid \".env\" file (example in \".env.example\" file)\n{error}")
    exit(1)

# Extracting the time ranges string id for the arguments choices
TIMERANGE_CHOICES = [timerange.value[0] for timerange in TimeRange]

# Default argument values
DEFAULT_TIMERANGE = TimeRange.TR_60M.value[0]
DEFAULT_OUTPUT_DIR = 'events'


def create_dir_if_not_exists(path: str):
    """Creates a directory if non-existent. If the provided path contains multiple level of directories, it will create them all.

    Args:
        path (str): Path of the directory
    """
    # Creating the directories path if non-existent
    os.makedirs(path, exist_ok=True)

    # If path already exists, checking that it is a valid directory
    if not os.path.isdir(path):
        print("Cannot create events output directory, a file with the same anem already exists")

    # If path already exists, checking that we have write permission
    if not os.access(path, os.W_OK):
        print(
            f'Cannot write in directory : {path}, permission denied', sys.stderr)
        exit(1)


def write_events_to_json_file(dir_path: str, event_type: str, events: list):
    """Writes the provided events to a ``.json`` file

    Args:
        dir_path (str): Directory path where to save the events file
        event_type (str): Events type, used to append them to the file name
        events (list): Events to save into file
    """
    if len(events) == 0:
        return

    # Retrieving the latest and
    lastest_event = events[0].get('timestamp')
    earliest_event = events[-1].get('timestamp')

    file_name = f"events_{event_type}_{str(earliest_event)}-{str(lastest_event)}.json"

    with open(dir_path + os.path.sep + file_name, 'w') as outfile:
        json_data = json.dumps(events)
        outfile.write(json_data)


def main():
    parser = ArgumentParser(
        description="Simple script to retrieve events from the Netskope Platform")

    parser.add_argument('-t', '--timerange', type=str,
                        help=f"time range of the events, default : {DEFAULT_TIMERANGE}",  choices=TIMERANGE_CHOICES, default=DEFAULT_TIMERANGE)

    parser.add_argument('-od', '--output-dir', type=str,
                        help=f"output dir, default is {DEFAULT_OUTPUT_DIR}", default=DEFAULT_OUTPUT_DIR)

    parser.add_argument('-fw', '--forward', action='store_true',
                        help=f"Forward events directly to the configured HTTP collector instead of saving them to .json files")

    parsed_args = parser.parse_args()

    # No need to check for API KEY, check is already done earlier in the program
    if parsed_args.forward and CORTEX_XDR_TENANT_API_URL is None:
        print("Error: forward argument set to send events to the Cortex HTTP Collector but no URL provided, please check your .env file", file=sys.stderr)
        exit(1)

    parsed_time_range = parsed_args.timerange
    time_range = None
    for range in TimeRange:
        if range.value[0] == parsed_time_range:
            time_range = range

    # time_range cannot be None at this point because of imposed choices in the parser
    output_dir = parsed_args.output_dir

    # Preparing the events destination folder
    create_dir_if_not_exists(output_dir)

    # Initializing API Instance
    netskope_api = NetSkopeAPI(NETSKOPE_TENANT_API_URL, API_KEY)

    # Retrieving events
    app_events = netskope_api.get_app_events(time_range)
    page_events = netskope_api.get_page_events(time_range)
    audit_events = netskope_api.get_audit_events(time_range)
    infra_events = netskope_api.get_infra_events(time_range)
    network_events = netskope_api.get_network_events(time_range)

    if parsed_args.forward:
        cortex_http_collector = CortexHTTPCollector(
            collector_url=CORTEX_XDR_TENANT_API_URL, authorization_token=CORTEX_XDR_TENANT_API_TOKEN)
        cortex_http_collector.send_json_data(app_events)
        cortex_http_collector.send_json_data(page_events)
        cortex_http_collector.send_json_data(audit_events)
        cortex_http_collector.send_json_data(infra_events)
        cortex_http_collector.send_json_data(network_events)

        print("Events sent to the Cortex HTTP Collector")

    else:

        # Writing all events to json files
        write_events_to_json_file(output_dir, 'application', app_events)
        write_events_to_json_file(output_dir, 'page', page_events)
        write_events_to_json_file(output_dir, 'audit', audit_events)
        write_events_to_json_file(output_dir, 'infrastructure', infra_events)
        write_events_to_json_file(output_dir, 'network', network_events)

        print(f"Events written to directory {output_dir}")


if __name__ == '__main__':
    main()
